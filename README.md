# Vim Ide
A script to setup vim as a development ide.

## Requirements
* pathogen.vim to install and initialize plugins automatically
* vim 8+
* rust-src (additional components installed with rustup)
* pyright (python lsp completion installed with npm)

## Languages
* rust
* python
* golang

## TODO
* java
* cpp
